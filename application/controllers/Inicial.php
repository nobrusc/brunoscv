<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Inicial extends CI_Controller {
	function __construct() {
        parent::__construct();
        $this->load->model("inicial_ml", "modelo");
    }

    public function index() {

    	$data['infos'] = $this->modelo->get_infos();
    	
		$this->load->view("inicial/header_vw");
		$this->load->view("inicial/index_vw", $data);
		$this->load->view("inicial/footer_vw");
    }

    function setJson(){
    	$this->load->view("inicial/header_vw");
		$this->load->view("inicial/set_json_vw");
		$this->load->view("inicial/footer_vw");
    }

    function getJson() {
    	//Ler os dados do Arquivo data.json
    	$file = file_get_contents("data.json");
		$json = json_decode($file, true);
		
		//Recebe  da view a quantidade de imagens para download
		$quantidade = $this->input->post("quantidade");
		

		$i = 0;
		foreach ($json["data"] as $j) {
		//Interrompe o foreach dependendo da quantidade escolhida
			if($i >= $quantidade)
				break;
  		//Separar os dados relevantes do Json
			$data['usuario']  = $j["caption"]["from"]["username"];
			$data['link']     = $j['link'];
			$data['tipo']     = $j['type'];      
			$data['thumb']    = $j['images']['thumbnail']['url'];
			$data['image']    = $j['images']['standard_resolution']['url'];
			$data['legenda']  = $j['caption']['text'];
		//Fazer upload da imagem para o servidor
			$url = $j['images']['thumbnail']['url'];
			$name = basename($url);
			file_put_contents("assets/img/thumbnails/$name", file_get_contents($url));
			$data['image_name'] = $name;
			$data['category'] = 'Sem categoria';
		//Insere os dados na tabela
			$this->modelo->set_infos_json($data);
		//Intera para a proxima condição do if 
			$i++;
		}
		//Redireciona para a página principal
		redirect('inicial');

    }

     // Salva uma informação no banco de dados
    function infos_salvar($id = false) {
        // Monta o array com dados a serem salvos
        	$data['usuario']  = $this->input->post("usuario");
			$data['link']     = $this->input->post("link");
			$data['tipo']     = $this->input->post("tipo");      
			$data['thumb']    = $this->input->post("thumb");
			$data['image']    = $this->input->post("image");
			$data['legenda']  = $this->input->post("legenda");
		//Fazer upload do link da imagem para o servidor
			$url = $this->input->post("thumb");
			$name = basename($url);
			file_put_contents("assets/img/thumbnails/$name", file_get_contents($url));
			$data['image_name'] = $name;
			$data['category'] = 'Sem categoria';
        if ($id) {
            $data["id"] = $id;
            $data['category'] =  $this->input->post("category");
            // Atualiza uma informação
            $this->modelo->update_infos($data);
        } else {
            // Salva uma nova informação no banco
            $this->modelo->set_infos_json($data);
        }
        // Redireciona para a página principal de informaçãos
        redirect("inicial/");
    }

    function infos_preencher(){

    	$data['infos'] = $this->modelo->get_infos();

    	$this->load->view("inicial/header_vw");
		$this->load->view("inicial/infos_salvar_vw", $data);
		$this->load->view("inicial/footer_vw");
    }

    function infos_editar($id){

    	$data['infos'] = $this->modelo->get_infos_editar($id);

    	$this->load->view("inicial/header_vw");
		$this->load->view("inicial/infos_editar_vw", $data);
		$this->load->view("inicial/footer_vw");
    }

    function makeJson(){
  
		$dir = 'sqlite:c:wamp/www/projeto/application/database/Teste.db';
		$db = new PDO($dir) or die("Não foi possível conectar ao Banco de dados!");
 
		//Definir a busca do db//
		$query = "SELECT * FROM json";
 
		foreach ($db->query($query) as $row) {	
			echo '<pre>';
			$json = json_encode($row);
			print_r($json);
		}
    }
}

