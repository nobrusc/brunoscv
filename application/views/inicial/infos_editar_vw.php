<div class="row no-gutter">
<div class="col-md-12 no-gutter">
	<nav class="navbar navbar-default">
		<h3 class="h3-padding"> Adicionar Novas Informações</h3>
	</nav>
</div>
<div class="col-md-12 no-gutter">
	<nav class="navbar navbar-default">
		<?php echo form_open_multipart('inicial/infos_salvar/'. $infos[0]->id);?>
      		<div class="row">
        		<div class="col-md-7">
        			<label> Usuário: 
          				<input type="text"  name="usuario" class="form-control" 
          				placeholder="Ex: TheUltimateUser" required="" value="<?php echo $infos[0]->usuario;?>">	
          			</label>
        		</div>
        		<div class="col-md-5">
        			<label> Link Instagram: 
          				<input type="text"  name="link" class="form-control" 
          				placeholder="Ex: https://instagram.com/p/2_c5N_SaYZ/" required="" value="<?php echo $infos[0]->link;?>">	
          			</label>
        		</div>
        	</div>
        	<div class="row">
        		<div class="col-md-5">
        			<label> Link Thumb: 
          				<input type="text"  name="thumb" class="form-control" 
          				placeholder="Ex: https://scontent.cdninstagram.com/hphotos-xfa1/..." required="" value="<?php echo $infos[0]->thumb;?>">	
          			</label>
        		</div>
        		<div class="col-md-5">
        			<label> Link Imagem Original: 
          				<input type="text"  name="image" class="form-control" 
          				placeholder="Ex: https://scontent.cdninstagram.com/hphotos-xfa1/..." required="" value="<?php echo $infos[0]->image;?>">	
          			</label>
        		</div>
        		<div class="col-md-2">
        			<label> Tipo do Arquivo: 
          				<input type="text"  name="tipo" class="form-control" placeholder="Ex: Imagem" required="" value="<?php echo $infos[0]->tipo;?>">	
          			</label>
        		</div>
        	</div>
          <div class="row">
            <div class="col-md-2 col-md-offset-3">
              <label> Thumb : 
                 <img class="thumbnail" src="<?php echo base_url().'assets/img/thumbnails/'. $infos[0]->image_name; ?>"> 
              </label>
            </div>
            <div class="col-md-4">
                <label>Categorias:
                  <select name="category" class="form-control">
                    <option value="<?php echo $infos[0]->category; ?>"><?php echo $infos[0]->category; ?></option>
                    <option value="<?php echo 'food'?>">Food</option>
                    <option value="<?php echo 'animal'?>">Animal</option>
                    <option value="<?php echo 'place' ?>">Place</option>
                    <option value="<?php echo 'person'?>">Person</option>
                    <option value="<?php echo 'object'?>">Object</option>                  
                  </select>
                </label>
            </div>
          </div>
        	<div class="row">
        		<div class="col-md-12">
        			<label> Legenda: 
          				<textarea  name="legenda" class="form-control" placeholder="" required=""><?php echo $infos[0]->legenda;?></textarea>	
          			</label>
        		</div>
        	</div>
        	<div class="row">
        		<div class="col-md-4 col-md-offset-3">
					<button type="button" class="btn btn-primary voltar"><< Voltar</button>
				</div>
				<div class="col-md-4 end">
					<button type="submit" class="btn btn-success">Editar</button>
				</div>
			</div>
      	<?php echo form_close();?>
	</nav>
<script type="text/javascript">
	$(document).ready(function(){
		$('.voltar').click(function(){
			window.location = "<?php echo site_url();?>/inicial/";
		});
	});
</script>

<style type="text/css">
	.form-control {
		margin-bottom: 0.8em !important;
	}
	label {
    	display: inline;
	}
</style>