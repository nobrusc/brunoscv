<!DOCTYPE HTML>
 <html lang="pt-br">
<head>
    <meta charset="utf-8"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">

    <title>Stories</title>
    <!-- Folhas de Estilo -->

    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet"/>
    <!-- <link href="<?php echo base_url();?>assets/css/jquery.dataTables.css" rel="stylesheet"/> -->
    <link href="<?php echo base_url();?>assets/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
    <link href="<?php echo base_url();?>assets/css/pagina.css" rel="stylesheet"/>
    <!-- Scripts -->
    <script src="<?php echo base_url();?>assets/js/jquery.min.js"/></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/dataTables.bootstrap.min.js"></script>
   
    <script src="<?php echo base_url();?>assets/js/modernizr.min.js"></script>
</head>
<body>

<div class="container">
    <div id="header">
        <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"></div>
            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8"></div>
        </div>
  <div class="row no-gutter">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <!--navbar -->
  <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Testes</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
          Stories<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Story 1</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Story 2</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Story 3</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Story 4</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Story 5</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Story 6</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Story 7</a></li>
          </ul>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</div>
<!--fim navbar -->