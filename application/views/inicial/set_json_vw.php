<div class="row no-gutter">
<div class="col-md-12 no-gutter">
	<nav class="navbar navbar-default">
		<h3 class="h3-padding">Carregar Json</h3>
	</nav>
</div>
<div class="col-md-12 no-gutter">
	<nav class="navbar navbar-default">
		<?php echo form_open_multipart('inicial/getJson/');?>
      		<div class="row">
        		<div class="col-md-12">
        			<label> Quantidade de Fotos: 
          				<input type="text"  name="quantidade" class="form-control" 
          				placeholder="Digite a Quantidade de Fotos que você deseja carregar Ex: 6" required="" pattern="[0-9]+$">	
          			</label>
        		</div>
          </div>
          <div class="row">
            <div class="col-md-4 col-md-offset-5">
             <button type="submit" class="btn btn-success">Carregar</button>
            </div>
          </div>
      <?php echo form_close();?>
  </nav>
</div>

<style type="text/css">
  .form-control {
    margin-bottom: 0.8em !important;
  }
  label {
      display: inline;
  }
</style>