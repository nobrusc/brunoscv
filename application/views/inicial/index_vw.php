<div class="row no-gutter">
<div class="col-md-12 no-gutter">
	<nav class="navbar navbar-default">
	<h3 class="h3-padding">Todos os Resultados</h3>
	</nav>
	<div class="col-md-12">
		<button type="button" class="btn btn-primary right mg-btn-bttm add">Add Novo</button>
		<button type="button" class="btn btn-success right mg-btn-bttm load">Carregar Json</button>
	</div>
	<table class="table table-striped table-bordered">
        <thead>
	          <tr>
	            <th class="text-center">Usuário</th>
	            <th class="text-center">Perfil</th>
	            <th class="text-center">Tipo Arquivo</th>
	            <th class="text-center">Thumb</th>
	            <th class="text-center">Categoria</th>
	            <th class="text-center">Legenda</th>
	            <th class="text-center">Ação</th>
	          </tr>
        </thead>
    	<tbody>
    		<?php 
			  foreach ($infos as $i) { ?>
	          <tr>
	            <td><?php  echo $i->usuario; ?></td>
	            <td><?php  echo $i->link;?></td>
	            <td><?php  echo $i->tipo;?></td>
	            <td><img src="<?php echo base_url().'assets/img/thumbnails/'. $i->image_name; ?>"></td>
	            <td><?php  echo $i->category;?></td>
	            <td><?php  echo $i->legenda;?></td>
	            <td><button type="button" class="btn btn-warning right mg-btn-bttm edit" id="<?php echo $i->id;?>">Editar</button></td>
	          </tr>
 			  <?php } ?>
   		</tbody>
 	</table>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.table').dataTable({
			"sScrollY": 340,
            "bScrollCollapse": true,
            "bPaginate": true,
            "bFilter": false,
            "oLanguage": {
					"sProcessing": "Aguarde enquanto os dados são carregados ...",
					"sLengthMenu": "Mostrar _MENU_ registros por página",
					"sZeroRecords": "Nenhum registro correspondente ao critério encontrado",
					"sInfoEmtpy": "Exibindo 0 a 0 de 0 registros",
					"sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
					"sInfoFiltered": "",
					"sSearch": "Procurar: ",
					"oPaginate": {
					   "sFirst":    "Primeiro",
					   "sPrevious": "Anterior",
					   "sNext":     "Próximo",
					   "sLast":     "Último"
					}
			}     
		});

		$('.add').click(function(){
			window.location = "<?php echo site_url();?>/inicial/infos_preencher/";
		});

		$('.load').click(function(){
			window.location = "<?php echo site_url();?>/inicial/setJson/";
		});

		$('.edit').click(function(){
		id = $(this).attr("id");
			window.location = "<?php echo site_url();?>/inicial/infos_editar/" + id;
		});
	});
</script>

