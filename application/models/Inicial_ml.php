<?php 
class Inicial_ml extends CI_Model {
    function __construtor() {
        parent::__construtor();
    }

    function get_infos(){

    	return $this->db->query("	SELECT * 
    								FROM json")
    					->result();
    }

    function get_infos_editar($id){
    	return $this->db->query("	SELECT * 
    								FROM json
    								WHERE id = '$id'")
    					->result();
    }

    function set_infos_json($data){
		return $this->db->insert("json", $data);
    }

     // Atualiza as informações no banco
    function update_infos($data){
       $this->db->where("id", $data["id"])
                ->update("json", $data);
    }
}