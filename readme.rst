
  O endereço para acesso do sistema é http://localhost/projeto
  O arquivo do SQL se encontra em application/database/Teste.db
  Feito com ajuda do Codeigniter

- A primeira story seria criar uma CLI e escolher a quantidade de fotos para fazer o download 
  e salvar essas informações no banco de dados SQLITE.
  A construção dessa story foi feita no controller INICIAL e na página inicial no botao 'Carregar Json'.
  lá está um input onde o usuário escolhe a quantidade de fotos a ser baixada e essa foto trás junto algumas 
  informações do json e popula a tabela.

- A segunda story seria uma ferramenta gráfica onde fosse possivel editar e adicionar novos itens.
  A construção dessa historia está na pagina inicial do controller INICIAL, onde lá possui os botoes de
  carregar o Json(1º story), Add novas informações e Editar as informações (2º story)

 -A terceira story seria construir uma REST API que leria os dados do banco de dados e criaria o arquivo
  com essa API.
  A construção dessa story está ligada a construção de outras stories anteriores. Está localizada na função makeJson
  do controller INICIAL, mas contém poucas informações, já que anteriormente foi consumida uma API já existente
  feito o sentido inverso dessa terceira story.

- A quarta story seria também uma interface grafica onde eu consumisse a api construida na story 3 
  e que fosse possivel categorizar as imagens dessa API. Como já foi consumida uma API existente
  ficaria redundante que o sistema consumisse uma nova API, ja que os dados são os mesmos, e no
  botão de editar é possivel categorizar as imagens e usar o makeJson para criar as novas informações
  da API com as categorias;

- As restantes não foram feitas.

Obrigado!